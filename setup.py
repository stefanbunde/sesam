from setuptools import find_packages, setup

from sesam import __version__


setup(
    name="sesam",
    version=__version__,

    author="Stefan Bunde",
    author_email="stefanbunde+git@gmail.com",

    packages=find_packages(),

    install_requires=[
        "pyperclip==1.7.0",
    ],

    classifiers=[
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.4",
    ],
)
