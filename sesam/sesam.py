import string
from getpass import getpass
from hashlib import pbkdf2_hmac

import pyperclip


ASCII_LETTERS = list(string.ascii_letters)
DIGITS = list(string.digits)
SPECIAL_CHARS = list("!\"#$%&'*/?@\\|~[]{}<>()-_+=;:,.")
PASSWORD_CHARACTERS = ASCII_LETTERS + DIGITS + SPECIAL_CHARS


def main():
    master_password = getpass("Master password: ")
    domain = getpass("Domain: ")

    hashed_bytes = pbkdf2_hmac("sha512",
                               (master_password + domain).encode("utf-8"),
                               "salt".encode("utf-8"),
                               4096)
    number = int.from_bytes(hashed_bytes, byteorder="big")
    password = []
    while number > 0 and len(password) < 10:
        password.append(PASSWORD_CHARACTERS[number % len(PASSWORD_CHARACTERS)])
        number = number // len(PASSWORD_CHARACTERS)

    pyperclip.copy("".join(password))
    print("copied password to clipboard!")


if __name__ == "__main__":
    main()
